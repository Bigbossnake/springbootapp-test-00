package com.spring.boot.code.coverage.SpringBootCodeCoverageDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootCodeCoverageDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCodeCoverageDemoApplication.class, args);
	}

}
